﻿using System;
using System.IO;
using System.Xml;

namespace DLink_REstart_POC
{
    /// <summary>
    /// Resets the ECC config file polling interval setting
    /// </summary>
    /// <remarks>
    /// This class is used to create a new ECC config.xml file that includes an exact copy of the old config.xml file 
    /// with the new polling interval set.
    /// </remarks>
    /// 
    public static class Config
    {
        public static string GetConfigItem(string configKey)
        {
            string fileName = @"DLinkRestartConfig.xml";
            string path = Path.Combine(Environment.CurrentDirectory, fileName);

            if (File.Exists(path))
            {
                //Create the XmlDocument.
                XmlDocument doc = new XmlDocument();
                doc.Load(path);

                //Query document for needed nodes
                XmlNode configValue = doc.GetElementsByTagName(configKey).Item(0);

                if (configValue != null)
                {
                    return configValue.InnerText;
                }
                else
                {
                    return "not found";
                }
            }
            else
            {
                throw new FileNotFoundException();
               
            }
        }
        
        
        /*
        /// <summary>
        /// Resets the ECC config file settings to point to Cloud (Angerona)
        /// </summary>
        /// <remarks>
        /// Parses the file c:\Program Files\NOC\config.xml file, locates the values for the PollingInterval, HostAddress, HostPort settings,
        /// and replaces these with the values sent to the method in a file configNew.xml in a folder also designated as a parameter.
        /// Additionally, the above elements will be created with the associated values if they do not exist. 
        /// JBM - 20160818
        /// There is a new requirement to remove the ExternalHostAddress element if it exists.
        /// If the ExternalHostAddress is pointed to noc.wayne.com, this is causing ECC to potentially try to execute the same
        /// task against both the legacy ECS and new Angerona platforms.  Removing this entry forces ECC to communicate only with Angerona. 
        /// </remarks>
        /// <param name="pollingInterval">The new value for the PollingInterval setting, in milliseconds</param>
        /// <param name="hostAddress">The new value for the HostAddress setting</param>
        /// <param name="hostPort">The new value for the HostPort setting</param>
        /// <param name="usingHttp">The new value for the UsingHttp setting</param>
        /// <param name="connectTimeout">The new value for the ConnectTimeout setting</param>
        /// <param name="newFileDirectory">the directory to place the new replacement file, configNew.xml</param>
        /// <returns>True on success, otherwise False if failed.  Success and failure markers are available in the log file.</returns>
        public static bool PointEccConfigToAzure(string pollingInterval, string hostAddress, string hostPort, string usingHttp, string connectTimeout, string newFileDirectory)
        {
            bool retVal = false;
            const string eccConfigFile = @"C:\Program Files\NOC\config.xml";                        // existing ECC config.xml file
            
            try
            {
                if (File.Exists(eccConfigFile))
                {
                    //Create the XmlDocument.
                    XmlDocument doc = new XmlDocument();
                    doc.Load(eccConfigFile);

                    //Query document for needed nodes
                    XmlNode clientConfigNode = doc.GetElementsByTagName("ClientConfig").Item(0);
                    XmlNode pollingIntervalNode = doc.GetElementsByTagName("PollingInterval").Item(0);
                    XmlNode hostAddressNode = doc.GetElementsByTagName("HostAddress").Item(0);
                    XmlNode hostPortNode = doc.GetElementsByTagName("HostPort").Item(0);
                    XmlNode usingHttpNode = doc.GetElementsByTagName("UsingHttp").Item(0);
                    XmlNode connectTimeoutNode = doc.GetElementsByTagName("ConnectTimeout").Item(0);
                    XmlNode externalHostAddress = doc.GetElementsByTagName("ExternalHostAddress").Item(0);
                    
                    //Check if <PollingInterval> exists, if not create it as the first child of <ClientConfig>, set new or existing value
                    if (pollingIntervalNode == null)
                        {
                            pollingIntervalNode = doc.CreateElement("PollingInterval");
                            if (clientConfigNode != null)
                            {
                                XmlNode clientConfigFirstChild = clientConfigNode.FirstChild;
                                clientConfigNode.InsertBefore(pollingIntervalNode, clientConfigFirstChild);
                            }
                        }
                    pollingIntervalNode.InnerText = pollingInterval;
                    
                    //Check if <HostAddress> exists, if not create it after <PollingInterval>, set new or existing value
                    if (hostAddressNode == null)
                        {
                            hostAddressNode = doc.CreateElement("HostAddress");
                            if (clientConfigNode != null)
                            {
                                clientConfigNode.InsertAfter(hostAddressNode, pollingIntervalNode);
                            }
                        }
                    hostAddressNode.InnerText = hostAddress;

                    //Check if <HostPort> exists, if not create it after <HostAddress>, set new or existing value
                    if (hostPortNode == null)
                    {
                        hostPortNode = doc.CreateElement("HostPort");
                        if (clientConfigNode != null)
                        {
                            clientConfigNode.InsertAfter(hostPortNode, hostAddressNode);
                        }
                    }
                    hostPortNode.InnerText = hostPort;

                    //Check if <ConnectTimeout> exists, if not create it after <HostPort>, set new or existing value
                    if (connectTimeoutNode == null)
                    {
                        connectTimeoutNode = doc.CreateElement("ConnectTimeout");
                        if (clientConfigNode != null)
                        {
                            clientConfigNode.InsertAfter(connectTimeoutNode, hostPortNode);
                        }
                    }
                    connectTimeoutNode.InnerText = connectTimeout;

                    // Per email from Matt 4/20/2016 the <UsingHttp> element does not need to be added if it does not exist
                    // However, if it does exist, we need to set the new value
                    if (usingHttpNode != null)
                    {
                        usingHttpNode.InnerText = usingHttp;
                    }

                    // Remove the <ExternalHostAddress> node if it exists
                    if (externalHostAddress != null)
                    {
                        // From the <ExternalHostAddress> we move up one level and delete the desired child node
                        XmlNode parentNode = externalHostAddress.ParentNode;
                        if (parentNode != null)
                        {
                            parentNode.RemoveChild(externalHostAddress);
                        }
                    }

                    //Save the working document as the new file
                    doc.Save(newFileDirectory + @"\configNew.xml");

                    retVal = true;

                    // Log success
                    SimpleLog.Info(@"File configNew.xml created in folder " + newFileDirectory + ".");
                }
                else
                {
                    SimpleLog.Warning("The ECC config.xml file was not found.");
                }
            }
            catch (Exception ex)
            {
                SimpleLog.Error("Attempt to create a new config.xml file with Angerona changes failed.");
                SimpleLog.Log(ex);
            }
           
            return retVal;
        }

        /// <summary>
        /// Checks that the client can connect to the Angerona web site.
        /// </summary>
        /// <remarks>
        /// Validates that the response is actually the from the Angerona site.
        /// Beware that a redirect is still a "successful" response.
        /// </remarks>
        /// <param name="url">The URI of the site to be validated.</param>
        /// <returns>True on success, otherwise False if failed.  Success and failure markers are available in the log file.</returns>
        public static bool IsWebsiteOnline(string url)
        {
            
            bool responseCheck = false;

            try
            {
                
                // We need to bypass the certificate check for this request
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
               
                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(url);
                myReq.Timeout = 10000;

                // We need to ensure the response is from Angerona - the request may have been redirected to another page, which is still a "successful" request.
                // The new Angerona setup should actively refuse the connection attempt.  
                // If there is no response, then the site is unreachable and we want the method to return false.
                // If there is a response, but the connection was actively refused (Forbidden), then the communication check was successful and we want to return true.
                try
                {
                    myReq.GetResponse();
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        HttpWebResponse response = (HttpWebResponse)ex.Response;
                        if (response.StatusCode == HttpStatusCode.Forbidden)
                        {
                            responseCheck = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    SimpleLog.Error(String.Format(@"Attempt to connect to {0} created an exception.", url));
                    SimpleLog.Log(ex);
                }

                // This is the old version when Angerona was configured with a splash page.  
                // Keep it for now in case Angerona setup changes.
                
         
                const string angeronaSplashPageComp = @"This web app has been successfully created";    // the string from the Angerona splash page to compare
                using (HttpWebResponse response = (HttpWebResponse)myReq.GetResponse())
                {
                    // We need to ensure the response is from Angerona - the request may have been redirected to another page, which is still a successful request
                    // Read the actual response stream and ensure it is the Angerona splash page
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                            {
                                var respStream =  sr.ReadToEnd();
                                SimpleLog.Info(String.Format(@"Response stream is {0}", respStream));
                                if (respStream.Contains(angeronaSplashPageComp))
                                {
                                    SimpleLog.Info(@"Response stream validated as Angerona splash page.");
                                    responseCheck = true;
                                }
                            }
                    
                    SimpleLog.Info(String.Format(@"IsWebsiteOnline method response status code is {0}.", response.StatusCode));
                    
                    return responseCheck && response.StatusCode == HttpStatusCode.Forbidden;
                }
                

            }
            catch (Exception ex)
            {
                SimpleLog.Error(@"IsWebsiteOnline method caught exception.");
                SimpleLog.Log(ex);
            }

            return responseCheck;

        }
    */
    }
}
