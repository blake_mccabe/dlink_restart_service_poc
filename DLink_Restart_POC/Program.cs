﻿using System;
using System.Net;
using DLink_REstart_POC;

//using System.Threading.Tasks;

namespace DLink_Restart_POC
{
    class Program
    {
        static void Main()
        {
            //
            // get configuration values
            string targetAddress1 = Config.GetConfigItem("TargetAddress1");

            // set up target address - Google DNS
            IPAddress targetAddress = IPAddress.Parse(targetAddress1);
            IPAddress defaultGateway = NetworkInfo.GetGatewayForDestination(targetAddress);

            // create a new telnet connection to DLink on port "23"
            TelnetConnection tc = new TelnetConnection(defaultGateway.ToString(), 23);


            // login with user "admin",password "admin" for testing,
            // PROD password is '"wayneix"
            // using a timeout of 100ms, and show server output
            string s = tc.Login("admin", "admin", 1000);
            Console.Write(s);

            // server output should end with "$" or ">", otherwise the connection failed
            // added "." for DLink
            string prompt = s.TrimEnd();
            prompt = s.Substring(prompt.Length - 1, 1);
            if (prompt != "$" && prompt != ">" && prompt != ".")
                throw new Exception("Connection failed");

            //prompt = "";
            string output;

            // while connected
            while (prompt != null && (tc.IsConnected && prompt.Trim() != "exit"))
            {
                // display server output
                Console.Write(tc.Read());

                // send client input to server
                prompt = Console.ReadLine();
                //prompt = "";
                tc.WriteLine(prompt);

                // display server output
                output = tc.Read();
                Console.Write(output);
            }
            Console.WriteLine("***DISCONNECTED");
            Console.ReadLine();
        }
    }
}
